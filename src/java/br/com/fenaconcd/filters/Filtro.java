/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.filters;

import br.com.fenaconcd.controllers.LoginController;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filtro responsavel por controlar as condicoes de permissao
 * @author Leonardo
 */
//@WebFilter(urlPatterns = "/restricted/*",servletNames = "{Faces Servlet}")
public class Filtro implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * Filtro que executa o fluxo de permissoes da aplicacao
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException 
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        LoginController loginMB = (LoginController) ((HttpServletRequest) request).getSession().getAttribute("loginController");
        
        if ((loginMB == null || !loginMB.isLoggedIn())) {
            String contextPath = ((HttpServletRequest) request).getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath + "/main/cadastroFuncionario.xhtml");
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }

}
