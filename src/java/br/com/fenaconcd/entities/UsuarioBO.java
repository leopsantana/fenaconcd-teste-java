/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.entities;

import br.com.fenaconcd.persistance.UsuarioFacade;
import br.com.fenaconcd.security.Senhas;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;

/**
 * Responsavel por representar a entidade de negocio Usuario e verificar os dados de login
 * @author Leonardo
 */
public class UsuarioBO implements Serializable{
    @EJB
    private UsuarioFacade uf;
    private List usuarios;

    public Usuario isUsuarioReadyToLogin(String login, String senha) {
        
        login = login.toLowerCase().trim();
         
        setUsuarios(getUf().findByLoginSenha(login, Senhas.encriptar(senha)));
        
        if (getUsuarios().size() == 1) {
            Usuario userFound = (Usuario) getUsuarios().get(0);
            return userFound;
        }

        return null;
    }

    public UsuarioFacade getUf() {
        return uf;
    }

    public void setUf(UsuarioFacade uf) {
        this.uf = uf;
    }

    public List getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List usuarios) {
        this.usuarios = usuarios;
    }

}
