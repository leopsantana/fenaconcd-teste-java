/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.entities;

import br.com.fenaconcd.security.Senhas;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

/**
 * Entidade de negocio para representar um Usuario
 *
 * @author Leonardo
 */
@Entity
@NamedQueries(value = {
    @NamedQuery(name = "Usuario.findByLoginSenha", query = "SELECT c FROM Usuario c " + "WHERE c.login = :login AND c.senha = :senha")})
public class Usuario implements Serializable {

    public static final long serialVersionUID = 0L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String nome;
    private String login;
    private String senha;
    @Transient
    public static final String FIND_BY_EMAIL_SENHA = "Usuario.findByEmailSenha";

    /**
     * Gera uma nova senha encriptada utilizando MD5
     *
     * @param senha - senha fonte
     */
    public void gerarSenha(String senha) {
        setSenha(Senhas.encriptar(senha));
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nome=" + nome + ", login=" + login + ", senha=" + senha + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.id);
        hash = 59 * hash + Objects.hashCode(this.nome);
        hash = 59 * hash + Objects.hashCode(this.login);
        hash = 59 * hash + Objects.hashCode(this.senha);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        Usuario usuario = (Usuario) object;
        if (!(object instanceof Usuario)) {
            return false;
        }
        if ((getId() == null && usuario.getId() != null) || (getId() != null
                && !getId().equals(usuario.getId()))) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
