/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.entities;

import br.com.fenaconcd.enums.Cargo;
import br.com.fenaconcd.enums.Situacao;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;


/**
 * Entidade de negocio para representar o Funcionario
 * @author Leonardo
 */
@Entity
public class Funcionario implements Serializable{
    
    public static final long serialVersionUID = 0L;
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String nome;
    private String CPF;
    private String endCompleto;
    private Cargo cargo;
    private Integer CargaHoraria;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAdmissao;
    private Situacao situacao;
    private Cargo supervisor;
    @OneToOne(cascade = CascadeType.ALL)
    private Ferias ferias;
    
    @Override
    public boolean equals(Object obj) {
       if (!(obj instanceof Funcionario)){
            return false;
       }
        Funcionario func = (Funcionario) obj;
        if ((getId() == null && func.getId() != null) || (getId() != null 
                && !getId().equals(func.getId())))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        hash = 67 * hash + Objects.hashCode(this.nome);
        hash = 67 * hash + Objects.hashCode(this.CPF);
        hash = 67 * hash + Objects.hashCode(this.endCompleto);
        hash = 67 * hash + Objects.hashCode(this.cargo);
        hash = 67 * hash + Objects.hashCode(this.CargaHoraria);
        hash = 67 * hash + Objects.hashCode(this.dataAdmissao);
        hash = 67 * hash + Objects.hashCode(this.situacao);
        hash = 67 * hash + Objects.hashCode(this.supervisor);
        return hash;
    }

    @Override
    public String toString() {
        return "Funcionario{" + "id=" + id + ", nome=" + nome + ", CPF=" + CPF + ", endCompleto=" + endCompleto + ", cargo=" + cargo + ", CargaHoraria=" + CargaHoraria + ", dataAdmissao=" + dataAdmissao + ", situacao=" + situacao + ", supervisor=" + supervisor + '}';
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF() {
        return CPF;
    }

    public void setCPF(String CPF) {
        this.CPF = CPF;
    }

    public String getEndCompleto() {
        return endCompleto;
    }

    public void setEndCompleto(String endCompleto) {
        this.endCompleto = endCompleto;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Integer getCargaHoraria() {
        return CargaHoraria;
    }

    public void setCargaHoraria(Integer CargaHoraria) {
        this.CargaHoraria = CargaHoraria;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public Cargo getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Cargo supervisor) {
        this.supervisor = supervisor;
    }

    public Ferias getFerias() {
        return ferias;
    }

    public void setFerias(Ferias ferias) {
        this.ferias = ferias;
    }

}
