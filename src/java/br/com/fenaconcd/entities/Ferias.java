/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 * Entidade de negocio para representar as Ferias
 * @author Leonardo
 */

@Entity
public class Ferias implements Serializable{
    
    public static final long serialVersionUID = 0L;
    
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataInicio;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataFim;
    @OneToOne(cascade = CascadeType.ALL)
    private Funcionario funcionario;
    private Integer periodoAquisitivo;
    
    
    @Override
    public boolean equals(Object obj) {
       if (!(obj instanceof Ferias)){
            return false;
       }
        Ferias ferias = (Ferias) obj;
        if ((getId() == null && ferias.getId() != null) || (getId() != null 
                && !getId().equals(ferias.getId())))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.dataInicio);
        hash = 47 * hash + Objects.hashCode(this.dataFim);
        hash = 47 * hash + Objects.hashCode(this.funcionario);
        hash = 47 * hash + Objects.hashCode(this.periodoAquisitivo);
        return hash;
    }

    @Override
    public String toString() {
        return "Ferias{" + "id=" + id + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", funcionario=" + funcionario + ", periodoAquisito=" + periodoAquisitivo + '}';
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Integer getPeriodoAquisitivo() {
        return periodoAquisitivo;
    }

    public void setPeriodoAquisitivo(Integer periodoAquisitivo) {
        this.periodoAquisitivo = periodoAquisitivo;
    }
    
    
    

    
}
