/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.enums;

/**
 * ENUM para representar os cargos existentes
 * @author Leonardo
 */
public enum Cargo {
    ESTAGIARIO("Estagiário"), TECNICO("Técnico"), ANALISTA("Analista"), GERENTE("Gerente");
    
    private String descricao;
    
    private Cargo(String descricao){
        setDescricao(descricao);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
}
