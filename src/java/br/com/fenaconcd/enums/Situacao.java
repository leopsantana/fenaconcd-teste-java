/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.enums;

/**
 * ENUM pare representar as situacoes existentes
 * @author Leonardo
 */
public enum Situacao {
    ATIVO("Ativo"), FERIAS("Em férias"), DESLIGADO("Desligado");
    
    private String descricao;

    private Situacao(String descricao){
        setDescricao(descricao);
    }
 
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
    
}
