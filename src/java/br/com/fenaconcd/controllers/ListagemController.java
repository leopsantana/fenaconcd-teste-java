package br.com.fenaconcd.controllers;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlDataTable;

/**
 * Controlador abstrato para controlador todas as listagem da apliacao
 * @author Leonardo
 */
@ManagedBean
@ViewScoped
public class ListagemController implements Serializable
{
    private boolean edicao;
   
    /**
     * Retorna a condição de edição da listagem.
     * @return false caso listagem estática, true caso edição em andamento
     */
    public boolean isEdicao(){
        return edicao;
    }
    /**
     * Avança uma tabela para a próxima página.
     * @param hdt - tabela
     */
    public void avancar(HtmlDataTable hdt){
        hdt.setFirst(hdt.getFirst() + hdt.getRows());
    }
    /**
     * Retrocede uma tabela para a página anterior.
     * @param hdt - tabela
     */
    public void retornar(HtmlDataTable hdt){
        hdt.setFirst(hdt.getFirst() - hdt.getRows());
    }
    /**
     * Define a condição de edição de uma página.
     * @param edicao - condição de edição
     */
    public void setEdicao(boolean edicao){
        this.edicao = edicao;
    }
}