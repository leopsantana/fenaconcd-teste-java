/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.controllers;

import br.com.fenaconcd.entities.Usuario;
import br.com.fenaconcd.entities.UsuarioBO;
import br.com.fenaconcd.utils.JsfUtil;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 * Controlador responsavel por gerenciar os dados de login do usuario
 * @author Leonardo
 */
@ManagedBean
@ViewScoped
public class LoginController implements Serializable{
    private boolean loggedIn;
    private Usuario usuarioLogado;
    private String login;
    private String senha;
    
    /**
     * Responsavel por autenticar o usuario no sistema
     * @return 
     */
    public String doLogin(){
                             
         Usuario usuarioFound = (Usuario) new UsuarioBO().isUsuarioReadyToLogin(login, senha);
                     
         if (usuarioFound == null){
               JsfUtil.addErrorMessage("Login ou senha errado(s). Tente novamente !");
               FacesContext.getCurrentInstance().validationFailed();
               return "/main/login.xhtml";     
         }else{
           loggedIn = true;
           usuarioLogado = usuarioFound;
           return "/restricted/cadastroUsuario.xhtml";
         }
    }
    
    /**
     * Responsavel por desautenticar o usuario do sistema
     * @return 
     */
    public String doLogout(){
    
         //Setamos a variável usuarioLogado como nulo, ou seja, limpamos
        //os dados do usuário que estava logado e depois setamos a variável
        //loggedIn como false para sinalizar que o usuário não está mais           
        //logado
         usuarioLogado = null;
         loggedIn = false;
         //Mostramos um mensagem ao usuário e redirecionamos ele para a         //página de login
         JsfUtil.addSuccessMessage("Logout realizado com sucesso !");
         return "/restricted/login.xhtml";
  }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    
    
}
