/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.controllers;

import br.com.fenaconcd.entities.Funcionario;
import br.com.fenaconcd.persistance.FuncionarioFacade;
import br.com.fenaconcd.utils.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlDataTable;

/**
 * Controlar responsavel por controlar a listagem dos dados funcionarios que possuem ferias para tirar mas ainda nao tiraram
 * @author Leonardo
 */
@ManagedBean
@ViewScoped
public class FeriasPendentesController implements Serializable{
    @EJB
    private FuncionarioFacade ff;
    private Collection<Funcionario> funcionariosFeriasPendentes;
    private Collection<Funcionario> allFuncionarios;
    private Funcionario funcionario;
    
    
    public FeriasPendentesController() {
        setFuncionario(new Funcionario());
    }
    
    /**
     * Anotacao @PostConstruct faz com que este metodo seja executado logo apos o construtor. 
     * Responsavel por popular a listagem de ferias pendentes fazendo uma consulta ao banco
     */
    @PostConstruct
    public void populate() {
        setAllFuncionarios(getFf().findAll());
        setFuncionariosFeriasPendentes(filtrarFuncionariosFeriasPendentes());
    }
    
    /**
     * Responsavel por verificar quais funcionarios possuem ferias para tirar mas ainda nao o fizeram
     */
    public Collection<Funcionario> filtrarFuncionariosFeriasPendentes(){
        Collection<Funcionario> funcionariosFeriasPendentes = new ArrayList<>();
        for (Funcionario funcionario : getAllFuncionarios()) {
            if (funcionario.getFerias() == null && isAnoPendente(funcionario) == false) {
                funcionariosFeriasPendentes.add(funcionario);
            }
        }
        return funcionariosFeriasPendentes;
    }
    
    /**
     * Responsavel por verificar o ano do periodo aquisitivo.
     * @param funcionario
     * @return 
     */
    public boolean isAnoPendente(Funcionario funcionario) {
        Date dataAdmissao = funcionario.getDataAdmissao();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataAdmissao);
        int ano = cal.get(Calendar.YEAR);
        int anoAtual = Calendar.getInstance().get(Calendar.YEAR);

        if (ano == anoAtual) {
            return true;
        } else {
            return false;
        }
    }
            
    /**
     * Responsavel por filtrar a listagem de ferias pendentes de acordo com o nome do funcionario
     */
    public void filtrar(){
        Collection<Funcionario> filtro = new ArrayList<>();
        String nomeFuncionario = getFuncionario().getNome();
        if (nomeFuncionario.isEmpty()){
            setFuncionariosFeriasPendentes(filtrarFuncionariosFeriasPendentes());
        }else{
            for (Funcionario func : getFf().findAll())
                if (func.getNome().toLowerCase().contains(nomeFuncionario.toLowerCase()))
                    filtro.add(func);
            setFuncionariosFeriasPendentes(filtro);
        }
    }
    
    /**
     * Responsavel por chamar o metodo que converte a data para que esta apareca no formato brasileiro
     * @param data
     * @return 
     */
    public String converterData(Date data){
        return JsfUtil.converterData(data);
    }
    
    /**
     * Responsavel por avancar uma pagina da listagem
     * @param hdt 
     */
    public void avancar(HtmlDataTable hdt) {
        hdt.setFirst(hdt.getFirst() + hdt.getRows());
    }

    /**
     * Responsavel por retornar uma pagina da listagem
     * @param hdt 
     */
    public void retornar(HtmlDataTable hdt) {
        hdt.setFirst(hdt.getFirst() - hdt.getRows());
    }
    
    //GETTERS AND SETTERS

    public void setFuncionariosFeriasPendentes(Collection<Funcionario> funcionariosFeriasPendentes) {
        this.funcionariosFeriasPendentes = funcionariosFeriasPendentes;
    }

    public Collection<Funcionario> getFuncionariosFeriasPendentes() {
        return funcionariosFeriasPendentes;
    }
    
    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public FuncionarioFacade getFf() {
        return ff;
    }

    public void setFf(FuncionarioFacade ff) {
        this.ff = ff;
    }  

    public Collection<Funcionario> getAllFuncionarios() {
        return allFuncionarios;
    }

    public void setAllFuncionarios(Collection<Funcionario> allFuncionarios) {
        this.allFuncionarios = allFuncionarios;
    }
    
    
}
