/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.controllers;

import br.com.fenaconcd.entities.Ferias;
import br.com.fenaconcd.entities.Funcionario;
import br.com.fenaconcd.persistance.FeriasFacade;
import br.com.fenaconcd.persistance.FuncionarioFacade;

import br.com.fenaconcd.utils.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

/**
 * Controlador responsável por controlar a persistência e listagem dos dados das férias dos funcionários.
 * @author Leonardo
 */
@ManagedBean
@ViewScoped
public class FeriasController implements Serializable {

    @EJB
    private FeriasFacade ff;
    @EJB
    private FuncionarioFacade funcf;
    private Collection<Ferias> feriasCollection;
    private Ferias ferias;
    private Funcionario funcionario;

    public FeriasController() {
        setFerias(new Ferias());
        setFuncionario(new Funcionario());
    }
    
    /**
     * Anotacao @PostConstruct faz com que este metodo seja executado logo apos o construtor. Responsavel por popular a listagem de ferias fazendo uma consulta ao banco 
     * e recuperar o funcionario colocado em sessao para atrelar as ferias com o respectivo funcionario.
     */
    @PostConstruct
    public void populate() {
        setFeriasCollection(getFf().findAll());
        recuperarFuncionarioSessao();
    }

    /**
     * Persiste no banco uma nova instancia de ferias ou informa ao usuario que ainda nao esta habilitado para tirar ferias.
     * @return Encaminha para a listagem das ferias ou atualiza a pagina informando o aviso.
     */
    public String cadastrar() {

        if (getFerias().getDataFim().before(getFerias().getDataInicio())) {
            JsfUtil.addErrorMessage("Data final não pode anteceder a data do início");
            return "/restricted/cadastroFerias.xhtml";
        } else {
            calcularPeriodoAquisitivo();
            getFf().create(getFerias());
            adicionarFuncionario();
            JsfUtil.addSuccessMessage("Cadastrado(a) com sucesso.");
            return "/restricted/listagemFerias.xhtml";
        }

    }

    /**
     * Edita uma instancia de Ferias no Banco de dados
     * @param ferias
     * @return Recarrega a pagina informando o aviso de sucesso
     */
    public String editar(Ferias ferias) {
        getFf().edit(ferias);
        JsfUtil.addSuccessMessage("Editado(a) com sucesso.");
        return "/restricted/listagemFerias.xhtml";
    }

    /**
     * Exclui uma instancia de Ferias do Banco de Dados
     * @param ferias
     * @return Recarrega a pagina informando o aviso de sucesso
     */
    public String excluir(Ferias ferias) {
        getFf().remove(ferias);
        JsfUtil.addSuccessMessage("Excluído(a) com sucesso.");
        return "/restricted/listagemFerias.xhtml";
    }

    /**
     * Preconiza o relacionamento @OneToOne ao persistir o Funcionario relativo a instancia de Ferias
     */
    public void adicionarFuncionario() {
        getFerias().setFuncionario(getFuncionario());
        getFf().edit(getFerias());
        adicionarFerias();
    }

    /**
     * Preconiza o relacionamento @OneToOne ao persistir as Ferias ao respectivo Funcionario
     */
    public void adicionarFerias() {
        getFuncionario().setFerias(getFerias());
        getFuncf().edit(getFuncionario());
    }

    /**
     * Para saber a qual Funcionario as Ferias pertencem foi necessario passar de uma pagina para outra via Sessao. 
     * Responsavel por recuperar da Sessao a instancia corrente
     */
    public void recuperarFuncionarioSessao() {
        Funcionario func = (Funcionario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("funcionario");
        setFuncionario(func);
    }
    
    /**
     * O periodo de aquisicao de ferias corresponde em um ano apos a data de admissao.
     * Responsavel por acrescer em um ano a data de admissao e inserir o periodo aquisitivo nas ferias
     */
    public void calcularPeriodoAquisitivo() {
        Date dataAdmissao = getFuncionario().getDataAdmissao();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataAdmissao);
        int ano = cal.get(Calendar.YEAR);
        getFerias().setPeriodoAquisitivo(ano + 1);
    }

    /**
     * Responsavel por avancar uma pagina da listagem
     * @param hdt 
     */
    public void avancar(HtmlDataTable hdt) {
        hdt.setFirst(hdt.getFirst() + hdt.getRows());
    }

    /**
     * Responsavel por retornar uma pagina da listagem
     * @param hdt 
     */
    public void retornar(HtmlDataTable hdt) {
        hdt.setFirst(hdt.getFirst() - hdt.getRows());
    }

    /**
     * Responsavel por chamar o metodo que converte a data para que esta apareca no formato brasileiro
     * @param data
     * @return 
     */
    public String converterData(Date data) {
        return JsfUtil.converterData(data);
    }
    
    //GETTERS AND SETTERS

    public FeriasFacade getFf() {
        return ff;
    }

    public void setFf(FeriasFacade ff) {
        this.ff = ff;
    }

    public Collection<Ferias> getFeriasCollection() {
        return feriasCollection;
    }

    public void setFeriasCollection(Collection<Ferias> feriasCollection) {
        this.feriasCollection = feriasCollection;
    }

    public Ferias getFerias() {
        return ferias;
    }

    public void setFerias(Ferias ferias) {
        this.ferias = ferias;
    }

    public FuncionarioFacade getFuncf() {
        return funcf;
    }

    public void setFuncf(FuncionarioFacade funcf) {
        this.funcf = funcf;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

}
