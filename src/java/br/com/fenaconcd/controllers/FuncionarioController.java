/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.controllers;

import br.com.fenaconcd.entities.Funcionario;
import br.com.fenaconcd.enums.Cargo;
import br.com.fenaconcd.enums.Situacao;
import br.com.fenaconcd.persistance.FuncionarioFacade;
import br.com.fenaconcd.utils.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlDataTable;
import javax.faces.context.FacesContext;

/**
 * Controlador responsável por controlar a persistência e listagem dos dados dos funcionarios
 * @author Leonardo
 */
@ManagedBean
@ViewScoped
public class FuncionarioController implements Serializable {

    @EJB
    private FuncionarioFacade ff;
    private Collection<Funcionario> funcionarios;
    private Funcionario funcionario;

    public FuncionarioController() {
        setFuncionario(new Funcionario());
    }

    /**
     * Anotacao @PostConstruct faz com que este metodo seja executado logo apos o construtor. 
     * Responsavel por popular a listagem dos funcionarios fazendo uma consulta ao banco 
     */
    @PostConstruct
    public void populate() {
        setFuncionarios(getFf().findAll());
    }

    /**
     * Persiste no banco uma nova instancia de funcionario ou informa ao usuario erro na populacao do supervisor.
     * @return Encaminha para a listagem dos funcionarios ou atualiza a pagina informando o aviso.
     */
    public String cadastrar() {
        cargoSemSupervisor();

        if (isSupervisorCorreto()) {
            getFf().create(getFuncionario());
            JsfUtil.addSuccessMessage(getFuncionario().getNome() + " cadastrado(a) com sucesso.");
            return "/restricted/listagemFuncionarios.xhtml";
        } else {
            return "/main/cadastroFuncionario.xhtml";
        }
    }

    /**
     * Responsavel por verificar o tipo de supervisor para cada cargo
     * @return 
     */
    public boolean isSupervisorCorreto() {
        boolean supervisorCorreto = true;

        switch (getFuncionario().getCargo().getDescricao()) {
            case "Estagiário":
                if (getFuncionario().getSupervisor() != Cargo.ANALISTA) {
                    JsfUtil.addErrorMessage("Somente um Analista pode supervisionar um Estagiário");
                    supervisorCorreto = false;
                }
                break;
            case "Técnico":
                if (getFuncionario().getSupervisor() != Cargo.ANALISTA) {
                    JsfUtil.addErrorMessage("Somente um Analista pode supervisionar um Estagiário");
                    supervisorCorreto = false;
                }
                break;
            case "Analista":
                if (getFuncionario().getSupervisor() != Cargo.GERENTE) {
                    JsfUtil.addErrorMessage("Somente um Gerente pode supervisionar um Analista");
                    supervisorCorreto = false;
                }
                break;
        }

        return supervisorCorreto;
    }

    /**
     * Edita uma instancia de Funcionario no Banco de dados
     * @param func
     * @return Recarrega a pagina informando o aviso de sucesso
     */
    public String editar(Funcionario func) {
        getFf().edit(func);
        JsfUtil.addSuccessMessage(func.getNome() + " editado(a) com sucesso.");
        return "/restricted/listagemFuncionarios.xhtml";
    }

    /**
     * Exclui uma instancia de Funcionario do Banco de Dados
     * @param func
     * @return Recarrega a pagina informando o aviso de sucesso
     */
    public String excluir(Funcionario func) {
        getFf().remove(func);
        JsfUtil.addSuccessMessage(func.getNome() + " excluído(a) com sucesso.");
        return "/restricted/listagemFuncionarios.xhtml";
    }

    /**
     * Responsavel por encaminhar para a tela onde as ferias serao cadastradas. Contudo, antes disso verifica se 
     * tal funcionario esta habilitado para tirar ferias. 
     * @param funcionario
     * @return Encaminha para cadastro de ferias ou recarrega a pagina informando o aviso
     */
    public String transferirParaCadastroFerias(Funcionario funcionario) {
        Date dataAdmissao = funcionario.getDataAdmissao();
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataAdmissao);
        int ano = cal.get(Calendar.YEAR);
        int anoAtual = Calendar.getInstance().get(Calendar.YEAR);

        if (ano == anoAtual) {
            JsfUtil.addErrorMessage("O funcionário ainda não pode tirar férias");
            return "/restricted/listagemFuncionarios.xhtml";
        } else {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("funcionario", funcionario);
            return "/restricted/cadastroFerias.xhtml";
        }
    }

    /**
     * Responsavel por chamar o metodo que converte a data para que esta apareca no formato brasileiro
     * @param data
     * @return 
     */
    public String converterData(Date data) {
        return JsfUtil.converterData(data);
    }
    
    /**
     * Responsavel por transformar o enum de representacao de um Cargo
     * @return Collection de cargo
     */
    public Collection<Cargo> getTiposCargo() {
        return Arrays.asList(Cargo.values());
    }

    /**
     * Responsavel por transformar o enum de representacao de um Situacao
     * @return Collection de situacao
     */
    public Collection<Situacao> getTiposSituacao() {
        return Arrays.asList(Situacao.values());
    }

    /**
     * Responsavel por avancar uma pagina da listagem
     * @param hdt 
     */
    public void avancar(HtmlDataTable hdt) {
        hdt.setFirst(hdt.getFirst() + hdt.getRows());
    }

    /**
     * Responsavel por retornar uma pagina da listagem
     * @param hdt 
     */
    public void retornar(HtmlDataTable hdt) {
        hdt.setFirst(hdt.getFirst() - hdt.getRows());
    }

    /**
     * Responsavel por anular o campo supervisor para um funcionario do tipo gerente, visto que esse nao possui supervisor
     */
    public void cargoSemSupervisor() {
        if (getFuncionario().getCargo() == Cargo.GERENTE) {
            getFuncionario().setSupervisor(null);
        }
    }

    /**
     * Responsavel por filtrar a listagem de funcionarios de acordo com o nome
     */
    public void filtrar() {
        Collection<Funcionario> filtro = new ArrayList<>();
        String nomeFuncionario = getFuncionario().getNome();
        if (nomeFuncionario.isEmpty()) {
            setFuncionarios(getFf().findAll());
        } else {
            for (Funcionario func : getFf().findAll()) {
                if (func.getNome().toLowerCase().contains(nomeFuncionario.toLowerCase())) {
                    filtro.add(func);
                }
            }
            setFuncionarios(filtro);
        }
    }

    
    //GETTERS AND SETTERS 
    
    public FuncionarioFacade getFf() {
        return ff;
    }

    public void setFf(FuncionarioFacade ff) {
        this.ff = ff;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Collection<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(Collection<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

}
