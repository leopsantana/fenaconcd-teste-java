/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.controllers;

import br.com.fenaconcd.entities.Usuario;
import br.com.fenaconcd.persistance.UsuarioFacade;
import br.com.fenaconcd.utils.JsfUtil;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * Controlar responsavel por cadastrar um novo usuario para o sistema
 * @author Leonardo
 */
@ManagedBean
@ViewScoped
public class UsuarioController implements Serializable{
    @EJB
    UsuarioFacade uf;
    private String senha;
    private Usuario usuario;
    
     public UsuarioController()
    {
        setUsuario(new Usuario());
    }
    
     /**
      * Cadastra um novo usuario
      * @return 
      */
    public String cadastrar()
    {
        getUsuario().gerarSenha(getSenha());
        getUf().create(getUsuario());
        JsfUtil.addSuccessMessage("Usuário cadastrado com sucesso.");
        return "/main/login.xhtml";
    }

    public UsuarioFacade getUf() {
        return uf;
    }

    public void setUf(UsuarioFacade uf) {
        this.uf = uf;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
    
}
