package br.com.fenaconcd.utils;


import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


/**
 * 
 * @author Leonardo
 */
public class JsfUtil
{
    /**
     * Adiciona uma mensagem de erro ao contexto.
     * @param message - mensagem de erro
     */
    public static void addErrorMessage(String message){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", " " + message));
    }
   
    /**
     * Adiciona uma mensagem de sucesso ao contexto.
     * @param message - mensagem de texto
     */
    public static void addSuccessMessage(String message){
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso:", " " + message));
    }
    
    /**
     * Converte um dado do tipo Date para o formato brasileiro
     * @param data
     * @return data formatada em PT-BR
     */
    public static String converterData(Date data){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dataFormatada = sdf.format(data);
        return dataFormatada;
    }
}