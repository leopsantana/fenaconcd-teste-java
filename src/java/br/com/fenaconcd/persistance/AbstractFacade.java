package br.com.fenaconcd.persistance;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;


/**
 * Modelo abstrato de classes de persistência.
 * @author Leonardo
 * @param <T> Generic Type
 */
abstract public class AbstractFacade<T>
{
    private Class<T> entityClass;

    
    /**
     * Define a classe da entidade gerenciada pela classe.
     * @param entityClass - classe da entidade
     */
    private void setEntityClass(Class<T> entityClass)
    {
        this.entityClass = entityClass;
    }
    /**
     * Retorna o gerenciador de entidade.
     * @return gerenciador de entidade
     */
    protected abstract EntityManager getEntityManager();
    
    /**
     * Instancia uma unidade de persistência com a classe informada.
     * @param entityClass - classe da entidade
     */
    public AbstractFacade(Class<T> entityClass)
    {
        setEntityClass(entityClass);
    }
   
    /**
     * Retorna a classe da entidade.
     * @return classe da entidade
     */
    public Class<T> getEntityClass()
    {
        return entityClass;
    }
    
    /**
     * Retorna a quantidade de registros na base de dados.
     * @return quantidade de registros
     */
    public int count()
    {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(getEntityManager().getCriteriaBuilder().count(cq.
                from(getEntityClass())));
        return ((Long) getEntityManager().createQuery(cq).getSingleResult()).
                intValue();
    }
    
    /**
     * Recupera todos os dados da base.
     * @return lista de objetos encontrados
     */
    public List<T> findAll()
    {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(getEntityClass()));
        return getEntityManager().createQuery(cq).getResultList();
    }
    
    /**
     * Encontra o objeto com o ID informado.
     * @param id - ID do objeto
     * @return objeto encontrado
     */
    public T find(Object id)
    {
        return getEntityManager().find(getEntityClass(), id);
    }
    
    /**
     * Insere um novo registro na base de dados.
     * @param entity - entidade a ser persistida
     */
    public void create(T entity)
    {
        getEntityManager().persist(entity);
    }
    
    /**
     * Edita o registro na base de dados.
     * @param entity - entidade a ser editada
     */
    public void edit(T entity)
    {
        getEntityManager().merge(entity);
    }
    
    /**
     * Remove a entidade da base de dados.
     * @param entity - entidade a ser removida
     */
    public void remove(T entity)
    {
        getEntityManager().remove(getEntityManager().merge(entity));
    }
    
    public TypedQuery<T> createNamedQuery(String query) {
        return getEntityManager().createNamedQuery(query, entityClass);
    }
     
}   