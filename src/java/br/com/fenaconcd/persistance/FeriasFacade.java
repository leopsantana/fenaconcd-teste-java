/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.persistance;

import br.com.fenaconcd.entities.Ferias;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * Unidade de persistência de ferias
 * @author Leonardo
 */
@Stateless
public class FeriasFacade extends AbstractFacade<Ferias>{
    @PersistenceContext
    private EntityManager em;
    
    
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * Instancia a persistência para a classe de ferias.
     */
    public FeriasFacade(){
        super(Ferias.class);
    }
    
    
}
