/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.persistance;

import br.com.fenaconcd.entities.Funcionario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Unidade de persistência de funcionario
 * @author Leonardo
 */
@Stateless
public class FuncionarioFacade extends AbstractFacade<Funcionario>{
    @PersistenceContext
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    /**
     * Instancia a persistência para a classe de funcionarios
     */
    public FuncionarioFacade() {
        super(Funcionario.class);
    }
 
}
