/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.fenaconcd.persistance;

import br.com.fenaconcd.entities.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author Leonardo
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario>{
    @PersistenceContext
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager(){
        return em;
    }
    
    public List<Usuario> findByLoginSenha(String login, String senha){
        return createNamedQuery("Usuario.findByLoginSenha").setParameter("login", login).setParameter("senha", senha).getResultList();
    }
    
    public UsuarioFacade(){
        super(Usuario.class);
    }
}
